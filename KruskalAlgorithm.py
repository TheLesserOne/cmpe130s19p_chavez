"""
For our project, we will be focusing on using the Kruskal algorithm to figure out the shortest path for laying down telephone or cable wires. The reasoning behind this is that we can use Kruskal’s algorithm to save telephone and cable companies a lot of money. Telephone and cable companies spend lots of money when they plan to lay down wires. Using Kruskal’s algorithm we can use the greedy approach to find a minimum spanning tree. Kruskal’s algorithm treats every node as an independent tree and will connect if it has the lowest cost compared to other options.

If companies can efficiently plan which paths are shorter and more economical, they can save more money. The reason this is important is, if you think about how much cable wires these companies use, it can amount to a large amount of money. The benefit of using the Kruskal algorithm can better help companies figure out which path will be cheaper

#vertice = v
"""
import time

def createSet(v):
    parent[v]=v
    rank[v]=0

def find(v):
    if parent[v]!=v:
        parent[v]=find(parent[v])

    return parent[v]

#does the union of two sets, in this case v 1 and v 2
def union(verticeOne, verticeTwo):
    rootOne=find(verticeOne)
    rootTwo=find(verticeTwo)

    if rootOne != rootTwo:
        if rank[rootOne]>rank[rootTwo]:
            parent[rootTwo]=rootOne
        else:
            parent[rootOne]=rootTwo

    if rank[rootOne]==rank[rootTwo]: rank[rootTwo] = rank[rootTwo] + 1

#kruskal algorithm implementation
def kruskal(graph):
    for v in graph['verticesInputs']:
        createSet(v)
        mst = set()
        edgeValues = list(graph['edgesInputs'])
        edgeValues.sort()

    for edgeValue in edgeValues:
        weight, verticeOne, verticeTwo = edgeValue
        if find(verticeOne) != find(verticeTwo):
            union(verticeOne, verticeTwo)
            mst.add(edgeValue)
    return sorted(mst)

parent = dict()
rank = dict()

#takes a list of cities and their distance in miles.
graph = {
'verticesInputs': [
  'San Jose', 
  'South San Francisco', 
  'Morgan Hill', 
  'Palo Alto', 
  'Fremont', 
  'Mountain View', 
  'Campbell'
  ],

'edgesInputs': set([
(47, 'San Jose', 'South San Francisco'),
(18, 'San Jose', 'Palo Alto'),
(47, 'South San Francisco', 'San Jose'),
(63, 'South San Francisco', 'Morgan Hill'),
(25, 'South San Francisco', 'Palo Alto'),
(36, 'South San Francisco', 'Fremont'),
(61, 'Morgan Hill', 'South San Francisco'),
(38, 'Morgan Hill', 'Fremont'),
(17, 'Palo Alto', 'San Jose'),
(25, 'Palo Alto', 'South San Francisco'),
(17, 'Palo Alto', 'Fremont'),
(8, 'Palo Alto', 'Mountain View'),
(36, 'Fremont', 'South San Francisco'),
(39, 'Fremont', 'Morgan Hill'),
(18, 'Fremont', 'Palo Alto'),
(23, 'Fremont', 'Mountain View'),
(22, 'Fremont', 'Campbell'),
(6, 'Mountain View', 'Palo Alto'),
(21, 'Mountain View', 'Fremont'),
(14, 'Mountain View', 'Campbell'),
(22, 'Campbell', 'Fremont'),
(13, 'Campbell', 'Mountain View'),
])
}
beginningTime = time.time()

print("MST contains the edges: {}".format(kruskal(graph)))
print("The minimum-spanning tree will have the following edges: {}".format(kruskal(graph)))

endingTime = time.time()
elapsedTime = endingTime-beginningTime

print("The run time for our minimum-spanning tree is: {}".format(elapsedTime))
